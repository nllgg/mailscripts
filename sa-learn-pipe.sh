#!/bin/bash
#
# Wrapper script for dovecot-antispam without using temporary files
# Look mom, no temporary files!
#
# Security is provided by locking the vmail user (dovecot-imap/antispam)
# only run this script via the sudoers line.  The script checks arguments
# to stay safe.  Log everything to syslog and return intelligent codes.
#
# sudoers:
#   vmail   ALL= (amavis) NOPASSWD: /etc/dovecot/sa-learn-pipe.sh
#
# dovecot/conf.d/90-antispam.conf:
#   plugin {
#     antispam_debug_target = syslog
#     #antispam_verbose_debug = 1
#     antispam_backend = pipe
#     antispam_trash = Trash
#     antispam_spam = Junk;Spam;SPAM
#     antispam_allow_append_to_spam = no
#     antispam_pipe_program = /etc/dovecot/sa-learn-pipe.sh
#     antispam_pipe_program_spam_arg = --spam
#     antispam_pipe_program_notspam_arg = --ham
#   }
#
if [ "$1" != "wrap" ]; then
        out=$(sudo -Hu amavis "$0" wrap "$@" < /proc/self/fd/0 2>&1)
        ret=$?
        lvl="info"
        if [ "$ret" -ne 0 ]; then
                lvl="err"
        fi
        logger -p mail.$lvl -i -t sa-learn-pipe "${1//[^a-z]/}: $out"
        exit $ret
fi

# sa-learn gets upset if in another user's home dir
cd "$HOME"

# Sanitize input to be only a-z, space and dash
cmd=${2//[^a-z\- ]/}

# Bash magic: turn stdin into a fd that sa-learn can read
sa-learn "$cmd" <(cat -)
ret=$?
exit $ret
